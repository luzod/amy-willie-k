<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * Methods for TimberHelper can be found in the /functions sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

	$context = Timber::get_context();

	// 
	// Set up selected articles header
	// ------------------------------------------------------------

// If it's set to random
		if (get_field('selection', 'option') == "random"):


			// get the trending topics
			$trending_args 							= array(
			  'order'                  	=> 'ASC',
			  'orderby'                	=> 'menu_order',
			  'post_type'              	=> 'nav_menu_item',
			  'post_status'            	=> 'publish',
			  'output'                 	=> ARRAY_A,
			  'output_key'             	=> 'menu_order',
			  'nopaging'               	=> true,
			  'update_post_term_cache' 	=> false );
			$trending_tags 							= wp_get_nav_menu_items( 'Trending Topics', $trending_args );

			// create an array for trending IDs
			$trending_ids 							= array();

			// push the ids to an array
			foreach( $trending_tags as $tag):
				$trending_ids[] = $tag->object_id;
				wp_reset_postdata();
			endforeach;


			// Get the posts
			$random_args = array(
		    'posts_per_page' => 3,
		    'post_type' 		 => 'post',
		    'orderby' 			 => 'rand',
		    'tag__in'				 => $trending_ids,
		    'order' 				 => 'DESC',
		    'date_query' 		 => array(
		        'after' 		 => date('Y-m-d', strtotime('-7 days')) 
		    )
			); 
			$context['selected_articles'] = query_posts($random_args);

		// Otherwise, just pull the stored order
		else:
			$context['selected_articles'] = get_field('article_list', 'option');
		endif;	

	Timber::render('404.twig', $context);