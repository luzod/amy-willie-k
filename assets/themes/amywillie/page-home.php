<?php
/**
 * Template name: Home
 * @package       WordPress
 * @subpackage    Timber
 * @since         Timber 0.1
 */

$context            = Timber::get_context();
$post               = new TimberPost();
$context['post']    = $post;
$context['gallery'] = get_field('gallery', 5);

// Get shows
date_default_timezone_set('Pacific/Honolulu');
$today = date('Ymd');
$show_args = array(
  'post_type'         => 'show',
  'posts_per_page'    => '13',
  'meta_key'          => 'show_date',
  'post_status'       => 'publish',
  'suppress_filters'  => true,
  'meta_query'        => array(
    array(
      'key'           => 'show_date',
      'value'         => $today,
      'compare'       => '>='
    )
  ),
  'orderby'           => 'meta_value_num',
  'order'             => 'ASC'
);
$context['shows']     = get_posts($show_args);

// Get news
$news_args = array(
  'posts_per_page'   => 2,
  'offset'           => 0,

  'orderby'          => 'post_date',
  'order'            => 'DESC',
  'post_type'        => 'post',
  'post_status'      => 'publish',
  'suppress_filters' => true 
);
$context['news']     = get_posts($news_args);
Timber::render('page-home.twig', $context);