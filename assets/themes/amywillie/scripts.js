var ajaxLoadMorePosts, updateNextCount;

updateNextCount = function() {
  var nextPage;
  if ($("#load-more").length > 0) {
    nextPage = $("#load-more").attr('href');
    return $.get(nextPage, function(data) {
      var nextSize;
      nextSize = $(data).find(".posts article").length;
      return $("#next-count").text(nextSize);
    });
  }
};

ajaxLoadMorePosts = function() {
  return $(document.body).on("click", "#load-more", function(e) {
    var myLink;
    e.preventDefault();
    $(this).addClass('loading').html('Loading');
    myLink = $(this).attr('href');
    $.get(myLink, function(data) {
      $(data).find(".posts article").appendTo(".posts");
      highlightTerms(term);
      return $("#load-more").removeClass('loading').html('Load <em></em>More');
    });
    return $("#pagination").load(myLink + " #load-more", function() {
      var newNextPage;
      newNextPage = $("#load-more").attr('href');
      return $.get(newNextPage, function(data) {
        var nextSize;
        nextSize = $(data).find(".posts article").length;
        return $("#next-count").text(nextSize);
      });
    });
  });
};

var breakPointConfig;

breakPointConfig = function() {
  return $("img.respond").breakpoint();
};

$(document).ready(function() {
  toggleMainNav();
  breakPointConfig();
  ajaxLoadMorePosts();
  return updateNextCount();
});

var toggleMainNav;

toggleMainNav = function() {
  return $('#toggle-nav, #nav-underlay, #close-nav').click(function(e) {
    e.preventDefault();
    return $('#main-nav').toggleClass('active');
  });
};
