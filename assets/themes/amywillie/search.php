<?php
/**
 * Search results page
 *
 * Methods for TimberHelper can be found in the /functions sub-directory
 *
 * @package 	WordPress
 * @subpackage 	Timber
 * @since 		Timber 0.1
 */

	$templates 									= array('search.twig');
	$context 										= Timber::get_context();
	$context['pagination'] 			= Timber::get_pagination();
	$context['title'] 					= get_search_query();
	$context['posts'] 					= Timber::get_posts();

	// get search results count
	$s													= trim($_GET['s']);

	// This works, comment out and save for later
	if($s != ""):
		$this_search 							= new WP_Query("s=" . trim($s) . "& showposts=-1");
		$result_count 						= $this_search->post_count;
		$context['results'] 			= $result_count;
		$context['is_search']			= is_search();
	endif;

	// Try breaking out query into one
	// $args = array(
	// 	'post_type'  => 'post',
	// 	'meta_query' => array(
	// 		array(
	// 			'key'     => 'authors',
	// 			'value'   => $s,
	// 			'compare' => 'LIKE',
	// 		),
	// 	),
	// );


	// $q1 = new WP_Query("s=" . trim($s) . "& showposts=-1");
	// $q2 = new WP_Query($args);

	// print_r($q2);
	

	// $q2 = get_posts(array(
	// 	'post_type' => 'post',
	// 	'meta_query' => array(
	// 		array(
	// 			'key' => 'authors',
	// 			'value' => $s,
	// 			'compare' => 'LIKE'
	// 		)
	// 	)
	// ));

	// $merged = array_merge( $q1, $q2 );

	// $post_ids = array();
	// foreach( $merged as $item ) {
 //    $post_ids[] = $item->ID;
	// }

	// $unique = array_unique($post_ids);

	// $posts = get_posts(array(
 //    'post_type' => 'posts',
 //    'post__in' => $unique,
 //    'post_status' => 'publish',
 //    'posts_per_page' => -1
	// ));

	// if( $posts ) : foreach( $posts as $post ) :
 	// 	setup_postdata($post);
	// enforeach; endif;

	// print_r($q1);

	// $context['posts']							= $posts;
	// $context['posts'] 					= Timber::get_posts();
	Timber::render($templates, $context);

