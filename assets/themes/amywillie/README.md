# Content Management for MMC / Brink

1. Navigation Social Media
2. Homepage
3. Posts
4. Trending Topics Header


## 1. Navigation and Social Media

### Navigation ###
All navigation elements are controlled from WordPress's standard menu editor. To view available menus, go to `Appearance -> Menus` and select which menu requires editing. The following menus have been hard-coded into the theme:

* __Primary__ : the main / category navigation
* __Trending Topics__ : secondary items below the main navigation
* __Supplemental__ : pages that show below trending topics, i.e. about, contact
* __Footer__ : similar to supplemental nav, but displayed in the footer and intended to show legal links as well.

To edit any of these, simply select the menu from the `Select a menu to edit` drop down and press the `Select` button. Any menu can accept pages, links to categories, links to tags and custom links. It is up to the editors to keep these grouped in a consistent manner. For example, there are no business rules in place to enforce tags only in the _Trending Topics_ nav.  

### Social Media ###
To manage links to Social Media profiles, go to `Options -> Social Media`. Inputs are available for Twitter and LinkedIn. If no value is present, the link will not display. Presently, we are limiting inputs to these two since a custom icon must be addded to our stylesheets for each profile.


## 2. Homepage

The featured articles on the homepage are controlled from the `Homepage` post in WP Admin. Go to `Pages->Home` to edit. The posts can be set as `manual` or `automatic`. In `automatic`, the four latest posts from trending topics are displayed in chronological order. In manual, you may select up to 4 posts and position them as-needed. 

There are three sections available, all are editable via drag and drop from a selection of articles. 

* __Block 1__, the main feature at the top of the homepage. 
* __Block 2__, two articles on the left side of the homepage (in desktop view) that are stacked vertically
* __Block 3__, a single post on the right side of the homepage (in desktop view) that has a dark blue background.

Please be sure to `save` your selections once they have been made.

## 3. Posts

To create or edit, go to `Posts` in WP Admin. Posts use standard WordPress post functionality. This includes a title, body, category and tags. Custom fields include: 

* __Article Type__ with options for `Standard`, `Video`, and `Data`. This flag determines whether or not the article's title in list view will show and icon, it does not affect the display of the article page itself. 
* __Image Setting__ with options for `Default`,`Alternate Featured` and `Hide Featured`. For `Default`, the featured image will display as a thumbnail in listing pages and as the intro image on the article page. For `Alternate Featured` an alternate image can be used at the top of the single article page, but the default featured image will still show as a thumbnail in listing pages. For `Hidden` the featured image will not show on single pages. 

The body should be edited using WordPress's WYSIWYG editor. This enables formatting as h1, h2, h3, blockquote, link and images.

## 4. Trending Topics Header

The trending topics header appears on multiple pages and is editable from the `Options` panel in WP Admin. To edit, go to `Options -> Trending Articles`. Two options are available, `Random` and `Curated`. 

* __Random__ draws from posts from the past 7 days. If viewing the module on a single article page, that article should be filtered from the selection so that it does not appear twice.  
* __Curated__ pulls three manually selected posts and displays in the order that is determined by the admin's drag and drop interface. 
* __Random from Trending__ draws three random posts that feature trending topic tags.