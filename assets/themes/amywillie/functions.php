<?php
	
	if (!class_exists('Timber')){
		echo 'Timber not activated. Make sure you activate the plugin in <a href="/wp-admin/plugins.php#timber">/wp-admin/plugins.php</a>';
		return;
	}

	class GridBase extends TimberSite {


		function __construct(){
			add_theme_support('post-formats');
			add_theme_support('post-thumbnails');
			add_theme_support('menus');
			add_filter('timber_context', array($this, 'add_to_context'));
			add_filter('get_twig', array($this, 'add_to_twig'));
			add_action('widgets_init', array($this, 'register_blog_sidebar'));
			// add_action('admin_enqueue_scripts', 'add_admin_scripts', 10, 1 );
			add_action('admin_enqueue_scripts', array($this, 'add_admin_scripts'));
			add_action('wp_enqueue_scripts', array($this, 'register_scripts'));
			add_action('init', array($this, 'register_post_types'));
			add_action('init', array($this, 'register_taxonomies'));			
			add_action('init', array($this, 'add_image_sizes'));

			// Hide Admin Bar
			add_filter('show_admin_bar', '__return_false');

			remove_action( 'wp_head', 'rsd_link');
			remove_action( 'wp_head', 'wlwmanifest_link');
			remove_action( 'wp_head', 'index_rel_link');
			remove_action( 'wp_head', 'parent_post_rel_link');
			remove_action( 'wp_head', 'start_post_rel_link');
			remove_action( 'wp_head', 'adjacent_posts_rel_link');
			remove_action( 'wp_head', 'wp_generator');
			remove_action( 'wp_head', 'feed_links', 2 );
			remove_action( 'wp_head', 'feed_links_extra', 3);
			remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );		

			Timber::add_route('package/:id', function($params) {
				$query = 'posts_per_page=1&post_type=special-package&p=' . $params['id'];
				Timber::load_template('ajax.php', $query, 200);
			});

			parent::__construct();
		}

		function add_image_sizes() {
			// Custom image sizes
			// use this instead of twig resize filter for consistency
			add_image_size('hero', 1400, 600, true );
			add_image_size('hero_tablet', 1024, 500, true );
			add_image_size('hero_handheld', 800, 600, true );

			add_image_size('hero_short', 1400, 300, true );
			add_image_size('hero_short_tablet', 1024, 200, true );
			add_image_size('hero_short_handheld', 800, 300, true );

			add_image_size('gallery_thumb', 300, 200, true );


			
		}

		function add_admin_scripts( $hook ) {
			global $post;

			if ( $hook == 'post-new.php' || $hook == 'post.php' ) {
				if ( 'post' === $post->post_type ) {     
					wp_enqueue_script(  'myscript', get_stylesheet_directory_uri().'/js/admin-author-populate.js' );
				}
			}
		}

		function register_scripts() {
			if($_SERVER['SERVER_NAME'] == 'local.mmcbrink.com') {
				wp_enqueue_script('livereload', 'http://localhost:35729/livereload.js', '', '', true );	
			}

			
			wp_register_style( 'style', get_template_directory_uri() . '/style.css','','', 'screen' );
			wp_enqueue_style( 'style' );

			// We are adding this to js-footer, but we can edit and use CDN if need be
			// Trying to minimize script calls, though
			wp_register_script( 'jquery-google', '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js', '', '1.11.1', true );
			wp_enqueue_script( 'jquery-google');

			wp_register_script( 'js-footer', get_template_directory_uri() . '/footer.js', '', '', true );
			wp_enqueue_script( 'js-footer' );	

			// Compile all scripts into one, not even modernizr needs to run in header
			wp_register_script( 'js-ready', get_template_directory_uri() . '/scripts.js', '', '', true );
			wp_enqueue_script( 'js-ready' );	
		}

		function register_post_types(){

			// Career Config
			$show_labels = array(
				'name'                => __( 'Show', 'text-domain' ),
				'singular_name'       => __( 'Show', 'text-domain' ),
				'add_new'             => __( 'Add new show', 'text-domain', 'text-domain' ),
				'add_new_item'        => __( 'Add new show', 'text-domain' ),
				'edit_item'           => __( 'Edit show', 'text-domain' ),
				'new_item'            => __( 'New show', 'text-domain' ),
				'view_item'           => __( 'View show', 'text-domain' ),
				'search_items'        => __( 'Search shows', 'text-domain' ),
				'not_found'           => __( 'No shows found', 'text-domain' ),
				'not_found_in_trash'  => __( 'No shows found in Trash', 'text-domain' ),
				'parent_item_colon'   => __( 'Parent show:', 'text-domain' ),
				'menu_name'           => __( 'Shows', 'text-domain' ),
			);

			$show_args = array(
				'labels'              => $show_labels,
				'hierarchical'        => false,
				'description'         => 'Authors',
				'taxonomies'          => array(),
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'show_in_admin_bar'   => true,
				'menu_position'       => null,
				'menu_icon'           => null,
				'show_in_nav_menus'   => true,
				'publicly_queryable'  => true,
				'exclude_from_search' => true,
				'menu_icon' 					=> 'dashicons-admin-site',
				'has_archive'         => true,
				'query_var'           => true,
				'can_export'          => true,
				'rewrite'             => true,
				'capability_type'     => 'post',
				'supports'            => array(
					'title', 'editor', 'author', 'thumbnail',
					'excerpt','custom-fields', 'trackbacks', 'comments',
					'revisions', 'page-attributes', 'post-formats'
					)
			);

			register_post_type( 'show', $show_args );
		}

		function register_taxonomies(){
			//this is where you can register custom taxonomies
		}

		function add_to_context($context){
			$context['nav_primary_a'] 		= new TimberMenu('Primary A');
			$context['nav_primary_b'] 		= new TimberMenu('Primary B');
			
			// Edit link - admins only
			$context['is_admin']					= current_user_can( 'manage_options' );

			$context['posts_per_page']		= get_option( 'posts_per_page' );
			$context['options'] 					= get_fields('options');
			$context['is_home']						= is_home();
			$context['site'] 							= $this;
			return $context;
		}


		function add_to_twig($twig){
				
			// Gravity form in twig
			$gravity_tag = new Twig_SimpleFilter('gravity_tag', function($fid) {
				return gravity_form(
					$fid, 
					$display_title=false, 
					$display_description=false, 
					$display_inactive=false, 
					$field_values=null, 
					$ajax=true
				);
			});

			// Checks for shortened title, returns if it exists 
			// and defaults to normal title if not
			$title_check = new Twig_SimpleFilter('title_check', function($pid) {

				if(get_field('short_title', $pid)):
					$post_title = get_field('short_title', $pid);
				else:
					$post_title = get_the_title($pid);
				endif;

				return $post_title;
			});


			// Checks for title value based on field and ID, if nothing, page title is used
			$meta_title_check = new Twig_SimpleFilter('meta_title_check', function($post_id, $field_name) {
				$title = get_field($field_name, $post_id);
				if($title):
					return $title;
				else:
					return get_the_title($post_id);
				endif;
			});

			// Checks for title value based on field and ID, if nothing, page description is used
			$desc_check = new Twig_SimpleFilter('desc_check', function($post_id, $field_name) {
				$desc = get_field($field_name, $post_id);
				$meta_desc = get_field('meta_description', $post_id);
				if($desc):
					return $desc;
				elseif(!$desc && $meta_desc):
					return $meta_desc;
				else:
					return bloginfo('description');
				endif;
			});

			// Checks for title value based on field and ID, if nothing, site description is used
			$meta_desc_check = new Twig_SimpleFilter('meta_desc_check', function($post_id, $field_name) {
				$meta_desc = get_field($field_name, $post_id);
				if($meta_desc):
					return $meta_desc;
				else:
					return bloginfo('description');
				endif;
			});

			// Checks for title value based on field and ID, if nothing, page touch icon is used
			$img_check = new Twig_SimpleFilter('img_check', function($post_id, $field_name) {
				$img = get_field($field_name, $post_id);
				if($img):
					return $img['url'];
				else:
					$touch_icon = get_field('touch_icons', 'options');
					return $touch_icon['url'];
				endif;
			});

			// Create the admin button
			$admin_button = new Twig_SimpleFilter('admin_button', function($post_id) {
				return get_edit_post_link($post_id, 'Edit');
			});

			// Get random image from gallery
			$random_from_gallery = new Twig_SimpleFilter('random_from_gallery', function(){

				// $rows = get_field('gallery', 5);
				// $row_count = count($rows);
				// $i = rand(0, $row_count - 1);
				// return $rows[ $i ];
				echo "test";
			});

			$twig->addFilter($admin_button);
			$twig->addFilter($title_check);
			$twig->addFilter($meta_title_check);
			$twig->addFilter($desc_check);
			$twig->addFilter($meta_desc_check);
			$twig->addFilter($img_check);
			$twig->addFilter($gravity_tag);
			$twig->addFilter($random_from_gallery);


			return $twig;

		}	
	}
	new GridBase();
