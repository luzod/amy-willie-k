<?php
/**
 * Template name: News Listing
 * @package       WordPress
 * @subpackage    Timber
 * @since         Timber 0.1
 */

$context              = Timber::get_context();


global $paged;
if (!isset($paged) || !$paged){
    $paged = 1;
}
$context = Timber::get_context();
$args = array(
    'posts_per_page'    => 3,
    'paged'             => $paged,
    'orderby'           => 'post_date',
    'order'             => 'DESC',
    'post_type'         => 'post',
    'post_status'       => 'publish',
    'suppress_filters'  => true 
);
query_posts($args);

$context['posts'] = Timber::get_posts();
$context['pagination'] = Timber::get_pagination();
$context['news_title'] = get_the_title(10);
$context['hero'] = get_field('hero_top', 10);
$context['news_intro'] = get_the_content(10);
Timber::render('page-news-listing.twig', $context);



