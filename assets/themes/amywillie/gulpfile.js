var gulp        = require('gulp'),
    gutil       = require('gulp-util'),
    uglify      = require('gulp-uglify'),
    watch       = require('gulp-watch'),
    concat      = require('gulp-concat'),
    notify      = require('gulp-notify'),
    livereload  = require('gulp-livereload'),
    sass        = require('gulp-ruby-sass'),
    dalek       = require('gulp-dalek')
    coffee      = require('gulp-coffee'),
    sourcemaps  = require('gulp-sourcemaps'),
    runSequence = require('run-sequence'),
    coffeelint  = require('gulp-coffeelint');


// SASS compile and compress
// -----------------------------------
gulp.task('sass', function () {

  // Compile and compress all styles - default
  gulp.src([
    './css/sass/style.sass',
    ])
    .pipe(sass({
      noCache: true,
      quiet: true,
      style: "compressed",
      sourcemap: true,
    }))
    .on('error', notify.onError("SASS compilation error"))
    .on('error', gutil.log)
    .pipe(livereload())
    .pipe(gulp.dest('./')
  );

  // Compile and compress ie styles
  // gulp.src([
  //   './css/sass/ie.sass',
  //   ])
  //   .pipe(sass({
  //     noCache: true,
  //     quiet: true,
  //     style: "compressed",
  //     sourcemap: true,
  //   }))
  //   .on('error', notify.onError("SASS compilation error"))
  //   .on('error', gutil.log)
  //   .pipe(livereload())
  //   .pipe(gulp.dest('./')
  // );

});


// CoffeeScript compile
// -----------------------------------
gulp.task('coffee', function() {
  gulp.src([
    './js/coffeescript/*.coffee'
    ])
    .pipe(coffee({bare: true, map: true})
    .on('error', gutil.log))
    .pipe(concat("scripts.js"))
    // .pipe(uglify())
    .pipe(gulp.dest('./'))
    .pipe(livereload());
});


// CoffeeScript lint and report
// -----------------------------------
gulp.task('coffee-lint', function(){
  return gulp.src('assets/javascript/coffeescript/*.coffee')
    .pipe(coffeelint())
    .pipe(coffeelint.reporter());
});


// JavaScript compile and compress
// -----------------------------------
gulp.task('js-header', function() {
  
  return gulp.src([
      './js/bower_components/modernizr/modernizr.js'
    ])
    .on('error', notify.onError("JavaScript compilation error"))
    .on('error', gutil.log)
    .pipe(sourcemaps.init())
     .pipe(concat("header.js"))
     .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./'))
    .pipe(livereload());
 
});

gulp.task('js-footer', function() {
  
  return gulp.src([
    // We use jQuery 1.11.1 since that's the last version compatible with 
    './js/libs/modernizr/modernizr.js',
    './js/libs/respond/dest/respond.src.js',
    './js/libs/breakpoint/breakpoint.js',
    './js/libs/magnific-popup/dist/jquery.magnific-popup.js',
    './js/libs/mobile-helper/mobile-helper.js'
    ])
    .on('error', notify.onError("JavaScript compilation error"))
    .on('error', gutil.log)
    .pipe(sourcemaps.init())
     .pipe(concat("footer.js"))
     .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./'))
    .pipe(livereload());
 
});


// Watch Task
// -----------------------------------
gulp.task('watch', function() {

  // SASS
  // -----------------------------------
  gulp.watch('css/sass/**/*.sass', function() {
    gulp.run('sass');
  });


  // CoffeeScript
  // -----------------------------------
  gulp.watch('./js/coffeescript/*.coffee', function() {
    runSequence('coffee', 'coffee-lint');
  });

  // Livereload
  // -----------------------------------
  var server = livereload();
  gulp.watch([
      './**/*.php',
      './**/*.twig',
      ]).on('change', function(file) {
      server.changed(file.path);
  });
});


// Put it all together
// -----------------------------------
// gulp.task('default', ['javascript','coffee-lint','coffee','watch']);
gulp.task('default', ['js-header', 'js-footer', 'watch']);
