updateNextCount = ->
  if($("#load-more").length > 0)
    nextPage = $("#load-more").attr('href')

    $.get nextPage, (data)->
      nextSize = $(data).find(".posts article").length
      $("#next-count").text(nextSize)

ajaxLoadMorePosts = ->
  $(document.body).on "click", "#load-more", (e) ->
    
    # prevent the click
    e.preventDefault()

    # add loading class
    $(this).addClass('loading').html('Loading')

    # get the link
    myLink = $(this).attr('href')


    # load new posts
    $.get myLink, (data)->
      $(data).find(".posts article").appendTo(".posts")
      highlightTerms(term)
      $("#load-more").removeClass('loading').html('Load <em></em>More')


    
    # update the link
    $("#pagination").load( myLink + " #load-more", -> 
      newNextPage = $("#load-more").attr('href')

      # get count from next posts
      $.get newNextPage, (data)->
        nextSize = $(data).find(".posts article").length
        $("#next-count").text(nextSize)
    )