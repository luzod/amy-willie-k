#
# Pulling all CoffeeScript into one file
# --------------------------------------------------

$(document).ready ->
  toggleMainNav()
  breakPointConfig()
  ajaxLoadMorePosts()
  updateNextCount()