<?php
/**
 * Template name: Calendar
 * @package       WordPress
 * @subpackage    Timber
 * @since         Timber 0.1
 */

$context              = Timber::get_context();
$post                 = new TimberPost();
$context['post']      = $post;

date_default_timezone_set('Pacific/Honolulu');
$today = date('Ymd');
$show_args = array(
  'post_type'         => 'show',
  'posts_per_page'    => '-1',
  'meta_key'          => 'show_date',
  'post_status'       => 'publish',
  'suppress_filters'  => true,
  'meta_query'        => array(
    array(
      'key'           => 'show_date',
      'value'         => $today,
      'compare'       => '>='
    )
  ),
  'orderby'           => 'meta_value_num',
  'order'             => 'ASC'
);
$context['shows']     = get_posts($show_args);

$past_show_args = array(
  'post_type'         => 'show',
  'posts_per_page'    => '-1',
  'meta_key'          => 'show_date',
  'post_status'       => 'publish',
  'suppress_filters'  => true,
  'meta_query'        => array(
    array(
      'key'           => 'show_date',
      'value'         => $today,
      'compare'       => '<='
    )
  ),
  'orderby'           => 'meta_value_num',
  'order'             => 'ASC'
);
$context['past_shows'] = get_posts($past_show_args);

Timber::render('page-calendar.twig', $context);



